# Tin Can Simple Statement Test

This is a very simple code sample to post an xAPI completion statement using TinCanJS library.  This is for troubleshooting CSOD's xAPI integration.

This is written in Typescript and uses the TinCanJS library.

I am using [Parcel](https://parceljs.org/) to build into a final Javascript bundle.

## Installation

`npm install`

## Test on localhost

`npm start`

This will run parcel in incremental mode, starting a development server on port 1234, and you can navigate to http://localhost:1234 to view it

When running locally like this, it will use a set of hard-coded credentials for SCORMCloud.  I have removed my user key and secret, but you can replace with your own SCORMCloud credentials for testing this way.

## Test on CSOD

`npm run-script build`

this will compile to a javascript bundle in the `dist` folder, and then zip up a file containing the contents of `dist` and the xAPI manifest (`tincan.xml`) file.

Upload this zip to CSOD using the Content Uploader

After doing this: 

* assign a "Provider"
* click "Publish" on the course page
* under "Availability", assign the course to your own User as "Suggested"
* in the drop down to the right of the course title,  "View as User"

When the window pops up it should have an editor where you can view/edit the json for the completion statement.  You can change it to use one of the other xAPI verbs.  Press Submit.

To verify that the statement was posted, use `Admin -> Tools -> Learning -> xAPI Statement Viewer`

View user transcript under `My Learning -> View Your Transcript`

## My Experiences

* First I tried without the convention `"http://www.cornerstoneondemand.com/xapi/lms/transcript_display"` under `content.contextActivities.category` (mentioned on p.31 Sec 10.1 of the `CSOD_xAPI_Support_-_Guidelines_v1.5.pdf`).  It succesfully posted the statement, but it does not appear on the transcript.
* When I added the special convention mentioned above I started getting error 400's ("Bad Request") on the statement post request, and the statement would not appear in the statement viewer.
* I eventually discovered that I could get rid of the error 400's by including additional information in the `object` part of the statement.  Initially I thought it would be sufficient just to list the `id` under the object but later found that adding the `definition.name` under the object would make these error 400's go away and allow it to post the statement

```
 "object": {
    "id": "http://wuzzo.ca/activity/tin-can-simple-statement-test",
    "definition": {
      "name": {
        "en-US": "tin-can-simple-statement-test"
      }
    }
  },
```

* When using both the `transcript_display` convention and the `object` with both `id` and `definition.name`, I was able to get the statement to show up on the transcript, but it does not seem to recognize it as being a completion of the the course. e.g. the course will still show up as "in Progress"

![1564642507233](assets/1564642507233.png)

And clicking on the hyperlink will show nothing on the details page indicating progress or completion:

![1564642738153](assets/1564642738153.png)

but at the same time the completion statement will show up under "Completed" and "Learning Records"

![1564642628657](assets/1564642628657.png)

the title on this will correspond to how the name is given in the `object.definition.name` part of the definition of the statement.  In this example here, the name used in the statement was hyphenated whereas the name used for the activity id in the `tincan.xml` was not, hence the descrepancy between the way that CSOD is displaying the course title versus the LRS record title.  But one would hope that CSOD would still recognize that the record's `object.id` is the same as the id from the `tincan.xml` file and therefore recognize the connection, but it does not seem to be doing that.

I did try making sure that the `object.definition.name` and the `tincan.xml` activity ID identical to see if that would solve my problems, but it still did not.

## Golf Sample Success

On the other hand, I believe that there is some way to get these completion statements to work properly because I modified the golf sample from https://xapi.com/download-prototypes/ and got it to work more properly on CSOD. 

Out of the box, this sample does not post a completion statement, so I needed to modify it.  I made the following modifications: 

* in `scripts/common.js` modified the `getContext` function to include the `transcript_display` convention that CSOD requires.
* in `assessmenttemplate.html`, added code to post completion statements:

```javascript
// CUSTOM: record "completed" with the special context that Cornerstone requires
function GetCompletionStatement(activityId, parentActivityId, isAssessment){
    verb = {
        id: "http://adlnet.gov/expapi/verbs/completed",
        display: {
            "en-US" : "completed"
        }
    }
    var stmt = {
        verb: verb,
        object: {
            id: activityId
        },
        context: GolfExample.getContext(parentActivityId, isAssessment)
    };

    return stmt;
}
```

You can find the modified version of the golf sample in the `GolfExample_TCAPI_Modified.zip` in the base of this repository.

When I uploaded this modified version to CSOD and took the quiz at the end of the course, it posted the xAPI completion statement and then the course on the transcript displayed as:

![1564643668588](assets/1564643668588.png)

and when clicking through into the details page it displayed three details indicating that the xAPI statements were working:  a status of "completed", a progress of "100%", and a score of "100%":

![1564643767323](assets/1564643767323.png)

This is the desired behaviour, but a behaviour I have been unable to replicate with my simpler example.

## Differences

I have been examining the differences between the Golf sample and my own sample, and between the xAPI completion statements that they create.

Here are some differences, but I'm not sure if any of these account for my sample not working:

* The golf sample posts a lot more xAPI statements, including the verbs `initialized` , `attempted` , `answered` (for each question on the quiz), `passed`, `completed`, and `terminated`.   I had a theory that CSOD might ignore the completion statement if it was not book-ended with `initialized` and `terminated` so I attempted to mimic this with my own sample (by manually editing the json when posting) but this did not solve the issue.
* The golf sample includes more information in the `contextActivities` section, specifically under `category` and `grouping`.  However, nothing in the `CSOD_xAPI_Support_-_Guidelines_v1.5.pdf` or other readings I have done about `contextActivities` online has given me the impression that any of this information should be a prerequisite for a successful completion statement.
* The golf sample's successful completion statement only contains the `id` of the course under the `object` section.  As mentioned previously, I was getting error 400's when trying to post completion statements which omitted the `object.definition` part, so it is not clear how the golf sample was able to do this successfully.
* In the golf sample's completion statement, The `transcript_display` part of the `contextActivities` is not visible when via the CSOD xAPI statement viewer, even though I adjusted the code to include this in the context.  

Below is a screenshot of a diff between the unsuccessful completion statement from my own code versus the successful one from the golf sample.

![1564644633206](assets/1564644633206.png)