
import TinCan from 'tincanjs'

import * as React from 'react'
import { render } from 'react-dom'
import "./index.less"
import AceEditor from 'react-ace'
import 'brace/mode/json';
import 'brace/theme/monokai';

function GetWindowQuery() {
    var match,
        pl = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query = window.location.search.substring(1);

    let urlParams = {};
    while (match = search.exec(query))
        urlParams[decode(match[1])] = decode(match[2]);

    console.log("urlParams", urlParams);
    return urlParams
}

class SimpleStatementTest {
    
    activityName = "tin-can-simple-statement-test-2";
    activityNameEnglish = "Tin Can Simple Statement Test"
    rootActivityId = "http://wuzzo.ca/activity/" + this.activityName;
    registration = "(no registration provided)";
    agent: TinCan.Agent;
    lrs: TinCan.LRS;
    statement: any;
    
    constructor() {
        const urlParams = GetWindowQuery();
        if ( urlParams["registration"] ) {
            this.registration = urlParams["registration"];
        } 

        // When the credentials are not provided on the URL, we'll fallback to a hard-coded set of credentials (for testing from localhost)
        // CSOD will pass the credentials on the URL when we upload this using the content uploader and view it as a user.
        let useHardCodedCredentials = false;
        if (!urlParams["endpoint"]) {
            useHardCodedCredentials = true;
        }
    
        console.log("Trying to connect to LRS")
        try {
            if (useHardCodedCredentials) {
                this.connectWithHardCodedCredentials();
            } else {
                this.connectWithCredentialsFromURL(urlParams);       
            }
        }
        catch (ex) {
            console.log("Failed to setup LRS object: ", ex);
            // TODO: do something with error, can't communicate with LRS
        }
        
        console.log("lrs", this.lrs);
    
        this.statement = this.createStatement("completed");
    }
    
    private connectWithCredentialsFromURL(urlParams: {}) {
        let actor: any = {};
        let actorString = urlParams["actor"];
        try {
            actor = JSON.parse(actorString);
        }
        catch (ex) {
            console.log("could not parse actor: ", ex);
        }
        // on scorm cloud it has \"accountName\":\"2BW01ZOVBE|uglycoyote@gmail.com\"
        let mbox = actor.mbox;
        if (!mbox) {
            console.log("no mbox, trying account", actor.account);
            if (actor.account && actor.account.length > 0) {
                let account = actor.account[0];
                console.log("first account", account);
                mbox = account.accountName.split('|')[1];
                console.log("using the mbox", mbox);
            }
        }
        this.agent = new TinCan.Agent({ mbox: mbox, name: actor.name });
        console.log("agent", this.agent);
        this.lrs = new TinCan.LRS({
            endpoint: urlParams["endpoint"],
            auth: urlParams["auth"],
            allowFail: false
        });
    }

    private connectWithHardCodedCredentials() {
        
        // Look at the SCORM Cloud LRS list page to get these.
        // Hard-coded credentials for testing on localhost and posting to the SCORMCloud LRS
        const uglyCoyoteCredentials = {
            endpoint: "https://cloud.scorm.com/lrs/2BW01ZOVBE/",
            mbox: "mailto:uglycoyote@gmail.com",
            key: "key", 
            secret: "secret",
            auth: undefined,
        };
        const currentUser = uglyCoyoteCredentials;
        let auth = "";
        if (currentUser.auth) {
            auth = currentUser.auth;
        }
        else {
            const key = currentUser.key;
            const secret = currentUser.secret;
            auth = 'Basic ' + btoa(key + ':' + secret);
        }
        this.agent = new TinCan.Agent({ mbox: currentUser.mbox });
        this.lrs = new TinCan.LRS({
            endpoint: currentUser.endpoint,
            auth: auth,
            allowFail: false
        });
    }

    createStatement(verb:string) {
        const statement =
        {
            actor: this.agent,
            verb: {
                id: "http://adlnet.gov/expapi/verbs/" + verb,
                display: {
                    "en-US": verb
                }
            },
            object: {
                id: this.rootActivityId,
                definition: {
                    name: {
                        "en-US": this.activityNameEnglish;
                    }
                }
            },
            context:{
                registration: this.registration,
                contextActivities:{
                    category:[
                        {"id":"http://www.cornerstoneondemand.com/xapi/lms/transcript_display" }
                    ]
                }
            },
        };
    
        return statement;
    }
    
    
    
    
    
    
}

let test = new SimpleStatementTest();

class Index extends React.Component<{},{statementJson:string}> {
    
    constructor(props:{}) {
        super(props)
        this.state = {statementJson:JSON.stringify(test.statement,null,4)};
    }

    render() {
        return <div>
            <h1>tin-can-simple-statement-test</h1>

            <p>This test just writes a completion statement to the LRS to test that this works.  Below is the content of the statement that will be submitted when you press the submit button.</p>
            
            <AceEditor
                mode="json"
                theme="monokai"
                value={this.state.statementJson}
                onChange={(newValue)=>this.setState({statementJson:newValue})}
                name="editorDiv"
                height="600px"
                width="90%"
            />

            <button onClick={()=>this.onSubmit()}>Submit!</button>
        </div>
    }

    onSubmit() {
        const statement = JSON.parse(this.state.statementJson);
        console.log("sending", statement);

        let tinCanStatement = new TinCan.Statement(statement);
        test.lrs.saveStatement(tinCanStatement, {callback:(err, result)=>{
            console.log("statement saved", err, result);
        }});    

    }
}

render( <Index/>, document.getElementById('contentRoot') );